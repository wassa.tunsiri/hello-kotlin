package com.example.todoapp.basic_api.ui

import com.example.todoapp.basic_api.model.BeerModel

interface BeerInterface {
    fun setBeer(beer: BeerModel)
}