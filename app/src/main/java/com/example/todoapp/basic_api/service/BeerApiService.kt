package com.example.todoapp.basic_api.service

import com.example.todoapp.basic_api.model.BeerModel
import retrofit2.http.GET
import retrofit2.Call

interface BeerApiService {

    @GET("v2/beers/random")
    fun getRandomBeer(): Call<List<BeerModel>>
}