package com.example.todoapp.basic_api.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.todoapp.R
import com.example.todoapp.basic_api.model.BeerModel
import com.example.todoapp.basic_api.service.BeerManager
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_beer.*


class BeerActivity: AppCompatActivity(), BeerInterface {

    private val presenter = BeerPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beer)
        presenter.getBeerApi()
        setView()
    }

    private fun setView() {
        btnRefresh.setOnClickListener {
            presenter.getBeerApi()
        }
    }

    override fun setBeer(beerItem: BeerModel) {
        beerTitle.text = beerItem.name
        beerDescription.text = beerItem.description
        beerAbv.text = beerItem.abv.toString()
        Picasso.get().load(beerItem.imageUrl).into(beerImage)
    }
}