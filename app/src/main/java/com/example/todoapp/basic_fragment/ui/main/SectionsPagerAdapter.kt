package com.example.todoapp.basic_fragment.ui.main

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.todoapp.R
import com.example.todoapp.basic_fragment.model.FragmentModel

private val TAB_TITLES = arrayOf(
    R.string.tab_text_1,
    R.string.tab_text_2
)

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(private val context: Context, private val fragmentModels: List<FragmentModel>, fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        return fragmentModels[position].fragment
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return fragmentModels[position].tabName
    }

    override fun getCount(): Int {
        // Show 2 total pages.
        return fragmentModels.count()
    }
}