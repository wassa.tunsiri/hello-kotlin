package com.example.todoapp.basic_fragment.model

import androidx.fragment.app.Fragment

data class FragmentModel(
    val tabName: String,
    val fragment: Fragment
)