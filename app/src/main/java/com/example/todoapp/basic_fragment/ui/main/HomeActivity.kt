package com.example.todoapp.basic_fragment.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.todoapp.R
import com.example.todoapp.basic_fragment.model.FragmentModel
import com.example.todoapp.basic_fragment.ui.main.PlaceholderFragment
import com.example.todoapp.basic_fragment.ui.main.ProfileFragment
import com.example.todoapp.basic_fragment.ui.main.SectionsPagerAdapter
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setView()
    }

    private fun setView() {
        val tabList: List<FragmentModel> = listOf<FragmentModel>(
            FragmentModel("Home", PlaceholderFragment.newInstance(1)),
            FragmentModel("Profile", ProfileFragment.newInstance())
        )

        val sectionsPagerAdapter = SectionsPagerAdapter(this, tabList, supportFragmentManager)
        viewPager.adapter = sectionsPagerAdapter
        tabs.setupWithViewPager(viewPager)
    }
}