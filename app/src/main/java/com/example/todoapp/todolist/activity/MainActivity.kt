package com.example.todoapp.todolist.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.todoapp.R
import com.example.todoapp.todolist.TodoAdapter
import com.example.todoapp.todolist.TodoClickListener
import com.example.todoapp.todolist.model.TodoListModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var todoAdapter: TodoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setView()
    }

    val listener = object : TodoClickListener {
        override fun onItemClick(model: TodoListModel) {

            TodoDetailActivity.startActivity(this@MainActivity, model.taskName, model)
            println("TodoClickListener onClick(" + model.taskName + ")")
        }
    }

    private fun setView() {
        todoAdapter = TodoAdapter(listener)

        recyclerView.adapter = todoAdapter
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.itemAnimator = DefaultItemAnimator()

        addButton.setOnClickListener {
            todoAdapter.addListItem(getModel(inputField.text.toString()))
            inputField.text = null
        }
    }

    private fun getModel(name: String): TodoListModel {
        return TodoListModel(false, name)
    }

}

