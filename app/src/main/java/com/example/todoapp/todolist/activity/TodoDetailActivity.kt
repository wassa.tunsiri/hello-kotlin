package com.example.todoapp.todolist.activity

import android.content.Intent
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.todoapp.R
import com.example.todoapp.todolist.model.TodoListModel
import kotlinx.android.synthetic.main.todo_detail.*

class TodoDetailActivity : AppCompatActivity() {

    companion object {
        const val TITLE = "TODOLIST_TITLE"
        const val MODEL = "TODOLIST_MODEL"

        fun startActivity(context: Context, titleName: String, model: TodoListModel) =
                context.startActivity(
                    Intent(context, TodoDetailActivity::class.java).also { myIntent ->
                        myIntent.putExtra(TITLE, titleName)
                        myIntent.putExtra(MODEL, model)
                    }
                )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.todo_detail)
        setView()
    }

    private fun setView() {
        todoDetail.text = intent.getStringExtra(TITLE)
        val model: TodoListModel = intent.getParcelableExtra(MODEL)

    }
}