package com.example.todoapp.todolist

import com.example.todoapp.todolist.model.TodoListModel

interface TodoClickListener {

    fun onItemClick(model: TodoListModel)

}