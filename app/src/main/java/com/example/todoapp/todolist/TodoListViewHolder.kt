package com.example.todoapp.todolist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp.R
import com.example.todoapp.todolist.model.TodoListModel

class TodoListViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.todo_item, parent, false)
)
{
    private val taskName: TextView = itemView.findViewById(R.id.todoItemTextView)
    private val taskCheckBox: CheckBox = itemView.findViewById(R.id.todoItemCheckBox)

    fun bind(model: TodoListModel, listener: TodoClickListener) {
        System.out.println("Bind() called")
        System.out.println(model.taskName)

        taskName.text = model.taskName

        itemView.setOnClickListener {
            listener.onItemClick(model)
        }

        taskCheckBox.setOnCheckedChangeListener(object: CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {

            }
        })

        itemView.visibility = if (model.isComplete) {
            View.GONE
        } else {
            View.VISIBLE
        }
    }
}