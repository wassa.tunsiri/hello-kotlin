package com.example.todoapp.todolist

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp.todolist.model.TodoListModel

class TodoAdapter(private val listener: TodoClickListener) : RecyclerView.Adapter<TodoListViewHolder>() {

    private val todoList: ArrayList<TodoListModel> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        TodoListViewHolder(parent)

    override fun onBindViewHolder(holder: TodoListViewHolder, position: Int) {
        holder.bind(todoList[position], listener)
        System.out.println("onBindViewHolder()")
        System.out.println(position)
    }

    override fun getItemCount(): Int = todoList.count()

    fun addListItem(item: TodoListModel) {
        todoList.add(item)
        System.out.println("addListItem() Called")
        System.out.println(item)
        notifyDataSetChanged()
    }
}



